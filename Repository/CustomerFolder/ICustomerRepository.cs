﻿using ReadingSQLscripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ReadingSQLscripts.Repository.CustomerFolder
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers();

        public Customer GetCustomerById(string id);

        public Customer GetCustomerByName(string name);

        public List<Customer> GetPageOfCustomer(int offset, int fetch);

        public bool AddNewCustomer(Customer customer);

        public bool UpdateCustomer(Customer customer);
    }
}
