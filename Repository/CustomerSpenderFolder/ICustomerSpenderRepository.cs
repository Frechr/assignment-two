﻿using ReadingSQLscripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingSQLscripts.Repository.CustomerSpenderFolder
{
    public interface ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetHighestSpenders();
    }
}
