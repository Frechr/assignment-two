﻿using Microsoft.Data.SqlClient;
using ReadingSQLscripts.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReadingSQLscripts.Repository.CustomerSpenderFolder
{
    public class CustomerSpenderRepository :ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerSpenderList = new List<CustomerSpender>();
            string sql = "SELECT CustomerId, Total FROM Invoice " +
                "GROUP BY CustomerId, Total " +
                "ORDER BY Total DESC; ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {                               
                                CustomerSpender customer = new CustomerSpender();
                                

                                customer.CustomerId = reader.GetInt32(0);
                                customer.Total = (double)reader.GetDecimal(1);

                                customerSpenderList.Add(customer);
                             
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerSpenderList;
        }
    }
}
