﻿using ReadingSQLscripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingSQLscripts.Repository.CustomerCountryFolder
{
    public interface ICustomerCountryRepository
    {
        public List<CustomerCountry> GetCustomersFromCountry();

    }
}
