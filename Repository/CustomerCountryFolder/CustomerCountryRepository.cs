﻿using Microsoft.Data.SqlClient;
using ReadingSQLscripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingSQLscripts.Repository.CustomerCountryFolder
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        public List<CustomerCountry> GetCustomersFromCountry()
        {
            List<CustomerCountry> customerCountryList = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(Country) AS Number FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY Number DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customer = new CustomerCountry();
                                customer.Country = reader.IsDBNull(0) ? null : reader.GetString(0);
                                customer.Number = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);

                                customerCountryList.Add(customer);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerCountryList;
        }
    }
}
