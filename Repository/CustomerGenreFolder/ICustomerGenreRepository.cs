﻿using ReadingSQLscripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingSQLscripts.Repository.CustomerGenreFolder
{
    public interface ICustomerGenreRepository
    {
        public CustomerGenre GetPopularGenreFromCustomer(int id);
    }
}
