﻿using Microsoft.Data.SqlClient;
using ReadingSQLscripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingSQLscripts.Repository.CustomerGenreFolder
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        public CustomerGenre GetPopularGenreFromCustomer(int id)
        {
            CustomerGenre customer = new CustomerGenre();
            string sql = "SELECT Genre.[Name], COUNT(Genre.[Name]) AS tracks FROM Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                "WHERE Customer.CustomerId = @CustomerId " +
                "GROUP BY Genre.[Name] " +
                "ORDER BY tracks ASC;";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.Name = reader.IsDBNull(0) ? null : reader.GetString(0);
                                customer.Track = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }
    }
}
