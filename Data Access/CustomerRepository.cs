﻿using assignment_two.Models;
using Kata7;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_two.Data_Access
{
    internal class CustomerRepository : IRepository<Customer>
    {
        public void AddRecord(Customer record)
        {
            throw new NotImplementedException();
        }

        public void DeleteRecord(int ID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Customer> GetAllRecords()
        {
            throw new NotImplementedException();
        }

        public void GetRecord(int ID)
        {
            throw new NotImplementedException();
        }

        public void UpdateRecord(Customer record)
        {
            throw new NotImplementedException();
        }
    }
}
