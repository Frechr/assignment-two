﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata7
{
    public interface IRepository<T>
    {
        private IEnumerable<T> Records { get => Records; set => Records = value; }
        public void AddRecord(T record);
        public void GetRecord(int ID);
        public void DeleteRecord(int ID);
        public void UpdateRecord(T record);
        public IEnumerable<T> GetAllRecords();
    }
}
