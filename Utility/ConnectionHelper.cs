﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClient_Demo.Repository
{
    public class ConnectionHelper
    {
        public static string GetConnectionString()
        {
            //ConnectionStringBuilder
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = @"N-NO-01-04-8024\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "CHINOOK"; //Database name
            connectStringBuilder.IntegratedSecurity = true;
            connectStringBuilder.Encrypt = false;
            return connectStringBuilder.ConnectionString;
        }
    }
}
